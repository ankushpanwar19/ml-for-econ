#%%
import numpy as np 
import os
import pandas as pd 

from sklearn.preprocessing import StandardScaler,PolynomialFeatures
from sklearn.model_selection import train_test_split
from sklearn.linear_model import LinearRegression,Ridge,Lasso
from sklearn.impute import SimpleImputer
from sklearn import linear_model, metrics, svm
import matplotlib.pyplot as plt
from sklearn.linear_model import ElasticNet
from sklearn.metrics import mean_squared_error, r2_score
from sklearn.kernel_ridge import KernelRidge

import torch
import torch.nn as nn
import torch.optim as optim
from torch.utils.data import Dataset, DataLoader
from tqdm import tqdm


from nn_regressor import RegressionDataset ,NNRegression,train_eval
from visualization import figure3,figure4,figure5,craa_calc

file_train = "peru_matlab_export_full.csv"
absolute_path = os.path.abspath(os.path.dirname(__file__))
data = pd.read_csv(os.path.join(absolute_path,file_train))
# data.drop(['training', 'h_hhsize', 'id_for_matlab', 'lncaphat_OLS','percapitahat_OLS', 'poor'], inplace = True, axis = 1)

# *********** Data Preprocessing ***********
def data_fillna(data,data_columns):

    columns_count=data[data_columns].sum()
    max_columnn=columns_count.keys()[columns_count.argmax()]
    data_columns.remove(max_columnn)
    data[max_columnn].fillna(1.0,inplace=True)
    for col in data_columns:
        data[col].fillna(0.0,inplace=True)

    return 0

wall_columns=['d_wall_other','d_wall_woodmat','d_wall_stonemud','d_wall_quincha','d_wall_tapia','d_wall_adobe','d_wall_stonecement','d_wall_brickcement']
data_fillna(data,wall_columns)

roof_columns=['d_roof_other','d_roof_straw','d_roof_mat','d_roof_platecane','d_roof_tile','d_roof_wood','d_roof_concrete']
data_fillna(data,roof_columns)

floor_columns=['d_floor_other','d_floor_earth','d_floor_cement','d_floor_wood','d_floor_tile','d_floor_sheets','d_floor_parquet']
data_fillna(data,floor_columns)

edu_columns=['d_h_educ_none','d_h_educ_pre','d_h_educ_prim','d_h_educ_sec','d_h_educ_higher_nouni','d_h_educ_higher_uni','d_h_educ_post']
data_fillna(data,edu_columns)

crowd_columns=['d_crowd_lessthan1','d_crowd_1to2','d_crowd_2to4','d_crowd_4to6','d_crowd_6plus']
data_fillna(data,crowd_columns)

data_train = data.loc[data['training']==1,:]
data_test = data.loc[data['training']==0,:]


X_train = data_train.iloc[:,1:73].to_numpy()
y_train = data_train.iloc[:,0].to_numpy()
X_test = data_test.iloc[:,1:73].to_numpy()
y_test = data_test.iloc[:,0].to_numpy()

label_poor = data_test['poor']

# scaler = PolynomialFeatures(2)
# scaler.fit(X_train)
# X_train=scaler.transform(X_train)
# X_test=scaler.transform(X_test)
# # X_submission=scaler.transform(X_submission)
#imputation for NAN

# *********** Linear Regression ***********
model = LinearRegression().fit(X_train, y_train)
pred_liner_regress = model.predict(X_test)
mse_liner_regress=mean_squared_error(y_test,pred_liner_regress)
R2score_liner_regress = model.score(X_test, y_test)
print("\n******** Linear regression ********")
print("MSE :",mse_liner_regress)
print("R2 score :",R2score_liner_regress)

# *********** Ridge Regression ***********
model = Ridge().fit(X_train, y_train)
pred_ridge = model.predict(X_test)
mse_ridge=mean_squared_error(y_test,pred_ridge)
R2score_ridge = model.score(X_test, y_test)
print("\n\n******** Ridge regression ********")
print("MSE :",mse_ridge)
print("R2 score :",R2score_ridge)

# *********** Lasso Regression ***********
model = Lasso(alpha=0.0001)
model.fit(X_train, y_train)
pred_lasso = model.predict(X_test)
mse_lasso=mean_squared_error(y_test,pred_lasso)
R2score_lasso = model.score(X_test, y_test)
print("\n\n******** Lasso regression ********")
print("MSE :",mse_lasso)
print("R2 score :",R2score_lasso)

# *********** Elastic Net Regression ***********
model = ElasticNet(random_state=42,alpha=0.0004)
model.fit(X_train, y_train)
pred_elastic = model.predict(X_test)
mse_elastic=mean_squared_error(y_test,pred_elastic)
R2score_elastic = model.score(X_test, y_test)
print("\n\n******** Elastic regression ********")
print("MSE :",mse_elastic)
print("R2 score :",R2score_elastic)

# *********** KernelRidge  Regression ***********
# model = KernelRidge()
# model.fit(X_train, y_train)
# pred_kernelridge = model.predict(X_test)
# mse_kernelridge=mean_squared_error(y_test,pred_kernelridge)
# R2score_kernelridge = model.score(X_test, y_test)
# print("\n******** kernelridge regression ********")
# print("MSE :",mse_kernelridge)
# print("R2 score :",R2score_kernelridge)

# *********** Neural Network ***********
print("\n******** Neural Net regression ********")
print("\n Training:")
train_dataset = RegressionDataset(torch.from_numpy(X_train).float(), torch.from_numpy(y_train).float())
test_dataset = RegressionDataset(torch.from_numpy(X_test).float(), torch.from_numpy(y_test).float())

NUM_FEATURES = X_train.shape[1]
device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")
model = NNRegression(NUM_FEATURES)
model.to(device)

EPOCHS = 100
BATCH_SIZE = 64
LEARNING_RATE = 0.001

train_loader = DataLoader(dataset=train_dataset, batch_size=BATCH_SIZE, shuffle=True)
test_loader = DataLoader(dataset=test_dataset, batch_size=1)

criterion = nn.MSELoss()
optimizer = optim.Adam(model.parameters(), lr=LEARNING_RATE)
sch=torch.optim.lr_scheduler.ExponentialLR(optimizer, 0.99, last_epoch=-1)

pred_nn=train_eval(model,train_loader,test_loader,criterion,optimizer,sch,EPOCHS,device)

mse_nn=mean_squared_error(y_test,pred_nn)
R2score_nn = r2_score(y_test,pred_nn)

print("MSE :",mse_nn)
print("R2 score :",R2score_nn)

# ************** Visualization *****************
y_pred_visual=np.array(pred_nn)
model_name='neuralnetwork'

figure3(y_test,y_pred_visual,model_name=model_name,precentile_limit=28)
benefits_per_household,inclusion_threshold,poor=figure4(y_test,y_pred_visual,label_poor,model_name=model_name)

hhsize=data_test.iloc[:,-5].to_numpy()
figure5(y_test,hhsize,y_pred_visual,label_poor,benefits_per_household,poor,inclusion_threshold,model_name=model_name)



# all CRAA figure in one graph
y_pred_visual1=np.array(pred_liner_regress)
y_pred_visual2=np.array(pred_ridge)
y_pred_visual3=np.array(pred_lasso)
y_pred_visual4=np.array(pred_nn)
hhsize=data_test.iloc[:,-5].to_numpy()
model_name=" "

benefits_per_household1,inclusion_threshold1,poor1=figure4(y_test,y_pred_visual1,label_poor,'linear regression',plot=False)
benefits_per_household2,inclusion_threshold2,poor2=figure4(y_test,y_pred_visual2,label_poor,'Ridge Regression',plot=False)
benefits_per_household3,inclusion_threshold3,poor3=figure4(y_test,y_pred_visual3,label_poor,'Lasso Regression',plot=False)
benefits_per_household4,inclusion_threshold4,poor4=figure4(y_test,y_pred_visual4,label_poor,'Neural Networks',plot=False)

# benefits_per_household,inclusion_threshold,poor=figure4(y_test,y_pred_visual,label_poor,model_name=model_name)
Craa_lr=craa_calc(y_test,hhsize,benefits_per_household1,poor1,inclusion_threshold1)
print("CRAA Utility for linear regression:",np.max(Craa_lr))
Craa_ridge=craa_calc(y_test,hhsize,benefits_per_household2,poor2,inclusion_threshold2)
print("CRAA Utility for Ridge regression:",np.max(Craa_ridge))
Craa_lasso=craa_calc(y_test,hhsize,benefits_per_household3,poor3,inclusion_threshold3)
print("CRAA Utility for Lasso regression:",np.max(Craa_lasso))
Craa_nn=craa_calc(y_test,hhsize,benefits_per_household4,poor4,inclusion_threshold4)
print("CRAA Utility for Neural Network:",np.max(Craa_nn))

plt.figure(1)
plt.title("Social Welfare versus Inclusion Error",fontsize=10)
plt.plot(inclusion_threshold1, Craa_lr, c = 'red',linewidth=0.6,label='Linear Regression')
plt.plot(inclusion_threshold2, Craa_ridge, c = 'blue',linewidth=0.6,label='Ridge Regression')
plt.plot(inclusion_threshold3, Craa_lasso, c = 'green',linewidth=0.6,label='Lasso Regression')
plt.plot(inclusion_threshold4, Craa_nn, c = 'orange',linewidth=0.6,label='Neural Network')

plt.xlabel("Inclusion Error")
plt.ylabel("CRRA Utility − Peru")
# plt.axvline(x=inclusion_threshold[max_idx], ls="--",c = 'black',linewidth=0.5)
plt.xlim(0,1)
plt.ylim(-0.3,-0.23)
plt.legend()

filepath="figures/figure5_"+'all'+".png"
plt.savefig(filepath)

summaryfile='PerformanceSummary.txt'
f=open(summaryfile, 'w')
f.write("Model Linear Regression: MSE:{:.5f} R2Score:{:.5f} CRAA:{:.5f} \n".format(mse_liner_regress,R2score_liner_regress,Craa_lr))
f.write("Model Ridge Regression: MSE:{:.5f} R2Score:{:.5f} CRAA:{:.5f}\n".format(mse_ridge,R2score_ridge,Craa_ridge))
f.write("Model Lasso Regression: MSE:{:.5f} R2Score:{:.5f} CRAA:{:.5f}\n".format(mse_lasso,R2score_lasso,Craa_lasso))
f.write("Model Neural Network: MSE:{:.5f} R2Score:{:.5f} CRAA:{:.5f}\n".format(mse_nn,R2score_nn,Craa_nn))

f.close()

print("end")
#%%