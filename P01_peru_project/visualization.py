import numpy as np 
import os
import pandas as pd 
import matplotlib.pyplot as plt
from sklearn.metrics import roc_curve
from tqdm import tqdm


def figure3(y_test,y_pred,model_name,precentile_limit=28):

    x_cutoff=np.percentile(y_pred,precentile_limit)
    y_cutoff=np.percentile(y_test,precentile_limit)
    plt.figure(1)

    plt.title("Predicted versus Actual per-capita Consumption for Households in Test Set Data",fontsize=10)
    plt.scatter(y_pred, y_test, c = 'b', s = 0.1)
    plt.axvline(x=x_cutoff, c = 'black')
    plt.axhline(y=y_cutoff, c = 'black')
    plt.xlabel("Predicted log per-capita monthly consumption")
    plt.ylabel("Actual log per-capita monthly consumption")
    plt.plot([2, 17], [2, 17], ls="--", c = '0.3')
    plt.xlim(2,10)
    plt.ylim(2,10)

    filepath="figures/figure3_"+model_name+".png"
    plt.savefig(filepath)
    # plt.show()

def figure4(y_test,y_pred,label_poor,model_name,plot=True):

    # for bring above poverity line
    fpr, tpr, thresholds = roc_curve(label_poor, y_pred, pos_label=0)
    
    inclusion_error=1-tpr
    tpr_poor=1-fpr

    plt.figure(1)
    plt.title("ROC Curves for Program Targeting (by Household)",fontsize=10)
    plt.plot(inclusion_error, tpr_poor, c = 'red')

    plt.xlabel("Inclusion Error")
    plt.ylabel("1-Exclusion Error")
    plt.plot([0, 1], [0, 1], ls="--", c = '0.3')
    plt.xlim(0,1)
    plt.ylim(0,1)
    
    if plot==True:
        filepath="figures/figure4a_"+model_name+".png"
        plt.savefig(filepath)
    # plt.show()

    national_num_households = 6750000
    program_budget_monthly = 274000000*3.21/12

    benefits_per_household=[]
    inclusion_threshold=[]
    y_pred_exp=np.expand_dims(y_pred,axis=1)
    thresholds_exp=np.expand_dims(thresholds,axis=0)

    poor=y_pred_exp<thresholds_exp
    count_poor=np.sum(poor,axis=0)
    mask=count_poor!=0
    count_poor=count_poor[mask]
    inclusion_threshold= inclusion_error[mask]

    percentage_poor=count_poor/y_pred.shape[0]
    benefits_per_household = program_budget_monthly/(national_num_households*percentage_poor)

    plt.figure(2)
    plt.title("Benefits per Household versus Inclusion Error for Different Eligibility Cutoffs",fontsize=10)
    plt.plot(inclusion_threshold, benefits_per_household, c = 'b')

    plt.xlabel("Inclusion Error")
    plt.ylabel("Per−household monthly benefits")
    plt.xlim(0,1)
    plt.ylim(0,120)

    filepath="figures/figure4b_"+model_name+".png"
    if plot==True:
        plt.savefig(filepath)

    return benefits_per_household,inclusion_threshold,poor[:,mask]
    # plt.show()

def figure5(y_test,hhsize,y_pred,label_poor,benefits_per_household,poor,inclusion_threshold,model_name):

    
    b= poor*benefits_per_household
    b= b.T/hhsize
    y_minus_b=(np.exp(y_test)+b)
    y_minus_b_pow=np.power(y_minus_b,-2)
    crra=np.sum(y_minus_b_pow,axis=1)/(-2)

    max_idx=np.argmax(crra)
    

    plt.figure(1)
    plt.title("Social Welfare versus Inclusion Error",fontsize=10)
    plt.plot(inclusion_threshold, crra, c = 'red',linewidth=0.8)

    plt.xlabel("Inclusion Error")
    plt.ylabel("CRRA Utility − Peru")
    plt.axvline(x=inclusion_threshold[max_idx], ls="--",c = 'black',linewidth=0.5)
    plt.xlim(0,1)
    plt.ylim(-0.3,-0.23)

    filepath="figures/figure5_"+model_name+".png"
    plt.savefig(filepath)

    # print("end")
    # plt.show()

def craa_calc(y_test,hhsize,benefits_per_household,poor,inclusion_threshold):

    b= poor*benefits_per_household
    b= b.T/hhsize
    y_minus_b=(np.exp(y_test)+b)
    y_minus_b_pow=np.power(y_minus_b,-2)
    crra=np.sum(y_minus_b_pow,axis=1)/(-2)
    
    return crra

